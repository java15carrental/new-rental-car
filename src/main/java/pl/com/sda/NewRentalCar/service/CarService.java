package pl.com.sda.NewRentalCar.service;

import java.util.List;

public interface CarService {
    List<Car> findAll();

    List<Car> newCars();

    Car findById(Long id);

    void save(Car car);
}
