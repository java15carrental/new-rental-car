package pl.com.sda.NewRentalCar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewRentalCarApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewRentalCarApplication.class, args);
	}

}
aaa